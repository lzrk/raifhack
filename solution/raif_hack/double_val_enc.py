import numpy as np
import pandas as pd
from typing import List

from sklearn.model_selection import StratifiedKFold, RepeatedStratifiedKFold, RepeatedKFold

# https://github.com/scikit-learn-contrib/category_encoders/blob/master/category_encoders/cat_boost.py
from .catboost import CatBoostEncoder


def get_single_encoder(encoder_name: str, cat_cols: list):
    """
    Get encoder by its name
    :param encoder_name: Name of desired encoder
    :param cat_cols: Cat columns for encoding
    :return: Categorical encoder
    """

    if encoder_name == "CatBoostEncoder":
        encoder = CatBoostEncoder(cols=cat_cols)

    return encoder


# https://miro.medium.com/max/2944/1*aFLhGR4GKfJJxZJlTR4ZLQ.png
# https://github.com/DenisVorotyntsev/CategoricalEncodingBenchmark/blob/master/src/utils.py
class DoubleValidationEncoderNumerical:
    """
    Encoder with validation within
    """
    def __init__(self, cols, encoders_names_tuple=()):
        """
        :param cols: Categorical columns
        :param encoders_names_tuple: Tuple of str with encoders
        """
        self.cols, self.num_cols = cols, None
        self.encoders_names_tuple = encoders_names_tuple

        self.n_folds, self.n_repeats = 5, 3
        self.model_validation = RepeatedKFold(n_splits=self.n_folds, n_repeats=self.n_repeats, random_state=0)
        self.encoders_dict = {}

        self.storage = None

    def fit_transform(self, X: pd.DataFrame, y: np.array) -> pd.DataFrame:
        self.num_cols = [col for col in X.columns if col not in self.cols]
        self.storage = []

        for encoder_name in self.encoders_names_tuple:
            for n_fold, (train_idx, val_idx) in enumerate(self.model_validation.split(X, y)):
                encoder = get_single_encoder(encoder_name, self.cols)

                X_train, X_val = X.loc[train_idx].reset_index(drop=True), X.loc[val_idx].reset_index(drop=True)
                y_train, y_val = y[train_idx], y[val_idx]
                _ = encoder.fit_transform(X_train, y_train)

                # transform validation part and get all necessary cols
                val_t = encoder.transform(X_val)
                val_t = val_t[[col for col in val_t.columns if col not in self.num_cols]].values

                if encoder_name not in self.encoders_dict.keys():
                    cols_representation = np.zeros((X.shape[0], val_t.shape[1]))
                    self.encoders_dict[encoder_name] = [encoder]
                else:
                    self.encoders_dict[encoder_name].append(encoder)

                cols_representation[val_idx, :] += val_t / self.n_repeats

            cols_representation = pd.DataFrame(cols_representation)
            # cols_representation.columns = [f"encoded_{encoder_name}_{i}" for i in range(cols_representation.shape[1])]
            cols_representation.columns = [f"encoded_{encoder_name}_{col}" for col in self.cols]
            self.storage.append(cols_representation)

        for df in self.storage:
            X = pd.concat([X, df], axis=1)

        X.drop(self.cols, axis=1, inplace=True)
        return X

    def transform(self, X: pd.DataFrame) -> pd.DataFrame:
        self.storage = []
        for encoder_name in self.encoders_names_tuple:
            cols_representation = None

            for encoder in self.encoders_dict[encoder_name]:
                test_tr = encoder.transform(X)
                test_tr = test_tr[[col for col in test_tr.columns if col not in self.num_cols]].values

                if cols_representation is None:
                    cols_representation = np.zeros(test_tr.shape)

                cols_representation = cols_representation + test_tr / self.n_folds / self.n_repeats

            cols_representation = pd.DataFrame(cols_representation)
            #cols_representation.columns = [f"encoded_{encoder_name}_{i}" for i in range(cols_representation.shape[1])]
            cols_representation.columns = [f"encoded_{encoder_name}_{col}" for col in self.cols]
            self.storage.append(cols_representation)

        for df in self.storage:
            X = pd.concat([X, df], axis=1)

        X.drop(self.cols, axis=1, inplace=True)
        return X