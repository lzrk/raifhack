import argparse
import logging.config

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from traceback import format_exc
from raif_hack.double_val_enc import DoubleValidationEncoderNumerical
from raif_hack.model import BenchmarkModel
from raif_hack.settings import MODEL_PARAMS, LOGGING_CONFIG, NUM_FEATURES, CATEGORICAL_OHE_FEATURES,CATEGORICAL_STE_FEATURES,TARGET
from raif_hack.utils import PriceTypeEnum
from raif_hack.metrics import metrics_stat
from raif_hack.features import prepare_categorical

logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger(__name__)


def parse_args():

    parser = argparse.ArgumentParser(
        description="""
    Бенчмарк для хакатона по предсказанию стоимости коммерческой недвижимости от "Райффайзенбанк"
    Скрипт для обучения модели
     
     Примеры:
        1) с poetry - poetry run python3 train.py --train_data /path/to/train/data --model_path /path/to/model
        2) без poetry - python3 train.py --train_data /path/to/train/data --model_path /path/to/model
    """,
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument("--train_data", "-d", type=str, dest="d", required=True, help="Путь до обучающего датасета")
    parser.add_argument("--model_path", "-mp", type=str, dest="mp", required=True, help="Куда сохранить обученную ML модель")

    return parser.parse_args()


if __name__ == "__main__":

    logger.info('START train.py')
    args = vars(parse_args())
    logger.info('Load train df')
    train_df = pd.read_csv(args['d'])
    logger.info(f'Input shape: {train_df.shape}')
    train_df = prepare_categorical(train_df)

    train_df[TARGET] = train_df[TARGET]

    X_train, X_test, y_train, y_test = train_test_split(
        train_df[NUM_FEATURES+CATEGORICAL_OHE_FEATURES+['date', 'street']+CATEGORICAL_STE_FEATURES+['price_type']],
        train_df[TARGET],
        stratify=train_df['price_type'],
        test_size=0.2,
        random_state=42
    )

    w_1, w_0 = (X_train.price_type.value_counts() / len(X_train)).tolist()
    logger.info(f"sample_weight: w_1={w_1}, w_0={w_0}")
    sample_weight = X_train.price_type.map(lambda x: w_1 if x == 1 else w_0)

    X_train = X_train.drop(columns=['price_type'])
    X_test = X_test.drop(columns=['price_type'])

    logger.info(f'X_offer {X_train.shape}  y_offer {y_train.shape}\tX_manual {X_test.shape} y_manual {y_test.shape}')

    X_train = X_train.reset_index(drop=True)
    y_train = y_train.reset_index(drop=True)
    X_test = X_test.reset_index(drop=True)
    y_test = y_test.reset_index(drop=True)

    model = BenchmarkModel(
        numerical_features=NUM_FEATURES,
        ste_categorical_features=CATEGORICAL_STE_FEATURES,
        model_params=MODEL_PARAMS,
        fit_args={"sample_weight": sample_weight}
    )

    logger.info('Fit model')
    model.fit(X_train, y_train, X_test, y_test)
    logger.info('Save model')
    model.save(args['mp'])

    # predictions_offer = model.predict(X_test)
    # metrics = metrics_stat(y_train.values, predictions_offer/(1+model.corr_coef)) # для обучающей выборки с ценами из объявлений смотрим качество без коэффициента
    # logger.info(f'Metrics stat for training data with offers prices: {metrics}')

    predictions_test = model.predict(X_test)
    metrics = metrics_stat(y_test.values, predictions_test)
    logger.info(f'Metrics stat for training data for TEST: {metrics}')
